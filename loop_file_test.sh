#!/bin/bash - 
#===============================================================================
#
#          FILE: loop_file_test.sh
# 
#         USAGE: ./loop_file_test.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Chad Fletcher 
#  ORGANIZATION: 
#       CREATED: 16/05/16 15:00
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

source ./loop_file_test-source.sh


loop_test $@

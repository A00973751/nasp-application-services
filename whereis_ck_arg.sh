#!/bin/bash - 
#===============================================================================
#
#          FILE: whereis_ck_arg.sh
# 
#         USAGE: ./whereis_ck_arg.sh 
# 
#   DESCRIPTION: script accepts the command name as an argument
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 04/18/2016 15:22
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error






declare locations 
declare whereis_output
declare cmd

which $1

cmd=$1

whereis_output=$(whereis $cmd)
locations=$( (echo $whereis_output | cut -d ":" -f 2-) )
if [[ "$locations" == "" ]]; then
        echo "$cmd is not available!"
        echo "Exit code: 127"
else
        echo "$cmd is located in $locations"
        echo "Exit code: 0"
fi


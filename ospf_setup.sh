#!/bin/bash - 
#===============================================================================
#
#          FILE: ospf_setup.sh
# 
#         USAGE: ./ospf_setup.sh 
# 
#   DESCRIPTION: OSPF Configuration 
#                
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "OSPF configuration starting"

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Student number invalid, please re-enter."
else
break
fi
done

echo "install quagga package"
yum install -y quagga

echo "change ownership"
chown quagga:quagga /etc/quagga

echo "configure zebra /etc/quagga/zebra.conf"

cat > /etc/quagga/zebra.conf <<EOF 
hostname zebra_router
password zebra
enable password zebra
log file /var/log/quagga/zebra.log
!
interface eth0
ip address 10.16.255.$StudentNumber/24
!
interface eth1
ip address 10.16.$StudentNumber.126/25
EOF

echo "enable and start zebra"
systemctl enable zebra
systemctl start zebra

echo "configure ospfd in /etc/quagga/ospfd.conf"

cat > /etc/quagga/ospfd.conf <<EOF
hostname ospfd
password zebra
log file /var/log/quagga/ospfd.log
!
interface eth0
interface eth1
!
router ospf
network 10.16.255.0/24 area 0
network 10.16.$StudentNumber.0/25 area 0
!
redistribute connected
EOF

echo " enable and start ospfd "
systemctl enable ospfd.service
systemctl start ospfd.service
systemctl status ospf.service

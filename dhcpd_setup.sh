#!/bin/bash - 
#===============================================================================
#
#          FILE: dhcpd_setup.sh
# 
#         USAGE: ./dhcpd_setup.sh 
# 
#   DESCRIPTION: DHCP setup
#
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "DHCP Config starting"

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Re-Enter Number"
else
break
fi
done

echo "Enabling IPv4 Forwarding"
cat > /etc/sysctl.conf <<EOF 
net.ipv4.ip_forward=1
EOF
echo "/sbin/sysctl -w net.ipv4.ip_forward=1 to enable it on run time" 
/sbin/sysctl -w net.ipv4.ip_forward=1


echo "Installing DHCP"
yum install -y dhcp

echo "Enabling DHCP Service"
cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/dhcpd.service
cat > /etc/systemd/system/dhcpd.service <<EOF
[Unit]
Description=DHCPv4 Server Daemon
Documentation=man:dhcpd(8) man:dhcpd.conf(5)
Wants=network-online.target
After=network-online.target
After=time-sync.target

[Service]
Type=notify
ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid eth1 wlp0s11u1

[Install]
WantedBy=multi-user.target
EOF

echo "DHCP Configuring"

cat > /etc/dhcp/dhcpd.conf <<EOF
   
option  domain-name     "rtr.s$StudentNumber.as2016.learn";
option  domain-name-servers 10.16.$StudentNumber.126;
option  domain-name-servers 142.232.221.253;

subnet 10.16.$StudentNumber.0 netmask 255.255.255.128 {
        option routers 10.16.$StudentNumber.126;
        range 10.16.$StudentNumber.100 10.16.$StudentNumber.125;

#Static Address for mail server
        host mail_server {
            hardware ethernet 08:00:27:A6:29:6E;
            fixed-address 10.16.$StudentNumber.1;
         }
		}

#Wireless Address DHCP Configuration
option  domain-name     "rtr.s$StudentNumber.as2016.learn";
option  domain-name-servers 10.16.$StudentNumber.126;
option  domain-name-servers 142.232.221.253;

subnet 10.16.$StudentNumber.128 netmask 255.255.255.128 {
	option routers 10.16.$StudentNumber.254;
        range 10.16.$StudentNumber.130 10.16.$StudentNumber.150;
        }
EOF

echo "Enabling and Restarting DHCP Service"
systemctl enable dhcpd.service
systemctl start dhcpd.service
systemctl status dhcpd.service

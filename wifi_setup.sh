#!/bin/bash - 
#===============================================================================
#
#          FILE: wifi_setup.sh
# 
#         USAGE: ./wifi_setup.sh 
# 
#   DESCRIPTION: WiFi setup 
#                
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "Wireless Configuration" 

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Invalid student number, re-enter."
else
break
fi
done

{
read -p "Enter student number"  BCIT_RegNumber
}

echo "Installing Hostapd" 
yum install -y hostapd

echo " Configuring hostapd for the wireless Network"
cat > /etc/hostapd/hostapd.conf <<EOF

ctrl_interface=/var/run/hostapd
ctrl_interface_group=wheel

# Some usable default settings...
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0

# Uncomment these for base WPA & WPA2 support with a pre-shared key
#wpa=3
#wpa_key_mgmt=WPA-PSK
#wpa_pairwise=TKIP
#rsn_pairwise=CCMP

# DO NOT FORGET TO SET A WPA PASSPHRASE!!
#wpa_passphrase=YourPassPhrase

# Most modern wireless drivers in the kernel need driver=nl80211
driver=nl80211

# Customize these for your local configuration...
interface=wlp0s11u1
hw_mode=g
channel=10
ssid=$BCIT_RegNumber
EOF

echo "Configuring IP address for interface wlp0s11u1"
cat > /etc/sysconfig/network-scripts/ifcfg-wlp0s11u1 <<EOF
DEVICE=wlp0s11u1
BOOTPROTO=none
TYPE=Wireless
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=10.16.$StudentNumber.254
PREFIX=25
ESSID=$BCIT_RegNumber
CHANNEL=10
MODE=Master
RATE=Auto
EOF

echo "Enabling & Starting Hostapd"
systemctl enable hostapd.service
systemctl start hostapd.service
systemctl status hostapd.service

echo "Enabling & Starting Network"
systemctl start network.service
systemctl status network.service

#!/bin/bash - 
#===============================================================================
#
#          FILE: mailserver_setup.sh
# 
#         USAGE: ./mailserver_setup.sh 
# 
#   DESCRIPTION: Mailserver setup
#                
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "Mail Server Configuration"

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Student number invalid, please re-enter."
else
break
fi
done

echo "Installing the Extra Packages for Enterprise Linux Repository" 
yum -y install epel-release
yum -y update

echo "Disabling Selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config

echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service
systemctl disable firewalld.service

echo "NTP" 
yum -y install ntp
timedatectl set-ntp true 
set ntpdate pool.ntp.org

echo "Configuring NTP"
cat > /etc/ntp.conf <<EOF
driftfile /var/lib/ntp/drift
restrict default nomodify notrap nopeer noquery
restrict 127.0.0.1
restrict ::1
server internationalserver1 iburst
server internationalserver2 iburst
server pool.ntp.org iburst
server 0.centos.pool.ntp.org iburst
server 1.centos.pool.ntp.org iburst
server 2.centos.pool.ntp.org iburst
server 3.centos.pool.ntp.org iburst
includefile /etc/ntp/crypto/pw
keys /etc/ntp/keys
disable monitor
EOF

echo "NTP" 
yum -y install telnet

echo "Postfix and Dovecot COnfig" 

#hostnamectl set-hostname mail.s$StudentNumberrtr.as2016.learn

echo " install dovecot daemons " 
yum install -y dovecot
yum install -y postfix

echo " Configure Dovecot on the Mail Server"
cat > /etc/dovecot/dovecot.conf <<EOF

## Dovecot configuration file

# Protocols we want to be serving.
protocols = imap pop3 lmtp


# "proxy::<name>".

dict {
  #quota = mysql:/etc/dovecot/dovecot-dict-sql.conf.ext
  #expire = sqlite:/etc/dovecot/dovecot-dict-sql.conf.ext
}


!include conf.d/*.conf

!include_try local.conf
EOF

echo "10-mail.conf"
cat > /etc/dovecot/conf.d/10-mail.conf<<EOF

mail_location = maildir:~/Maildir

namespace inbox {
 inbox = yes
}
mbox_write_locks = fcntl
EOF

echo "10-auth.conf"
cat > /etc/dovecot/conf.d/10-auth.conf <<EOF

disable_plaintext_auth = no
auth_mechanisms = plain
!include auth-system.conf.ext

EOF
cd /etc
echo " Configuring postfix on Mail Server"
cat > /etc/postfix/main.cf <<EOF
# Global Postfix configuration file. This file lists only a subset
# of all parameters. For the syntax, and for a complete parameter
# list, see the postconf(5) manual page (command: "man 5 postconf").
#
queue_directory = /var/spool/postfix
command_directory = /usr/sbin
daemon_directory = /usr/libexec/postfix
data_directory = /var/lib/postfix
mail_owner = postfix
inet_interfaces = all
inet_protocols = all
mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
unknown_local_recipient_reject_code = 550
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
home_mailbox = Maildir/
debug_peer_level = 2
debugger_command =
         PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
         ddd $daemon_directory/$process_name $process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
#
#ADDED SSL Lines
smtpd_use_tls = yes
smtpd_tls_key_file  = /etc/pki/tls/private/s01.as2016.learn.key
smtpd_tls_cert_file = /etc/pki/tls/certs/s01.as2016.learn.crt
smtpd_tls_loglevel = 3
smtpd_tls_received_header = yes
smtpd_tls_session_cache_timeout = 3600s
tls_random_source = dev:/dev/urandom
EOF

echo "Enabling & Starting Dovecot Services"
systemctl enable dovecot.service
systemctl start dovecot.service
systemctl status dovecot.service

echo "Enabling & Starting Postfix"
systemctl enable postfix.service
systemctl start postfix.service
systemctl status postfix.service

echo "Setting up Users"
useradd -m -G wheel,users olu
echo "olu ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
mkdir ~olu/.ssh/
cat > ~olu/.ssh/authorized_keys <<EOF
ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBACbbA/5CA4Z5AhmOX4JMyxqXIh7JwR7B6S0DOFCj4k8Y8255K/bGXWw5tFWokSCi+89wnj7Y5AIrEnhMo9Pp2y3iQG21hFs+Ba0KI7cSL73X4bUBhLy1EUZjo5wNcPTNG1YgG94a9iTIoqUtoZLRiDvmPMvNR929dOTD5UEA3t3wy2XXg== nasp@milkplus
EOF
chown -R olu:olu ~olu/.ssh
useradd -m -G wheel,users mide
echo "mide ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
mkdir ~mide/.ssh/
cat > ~mide/.ssh/authorized_keys <<EOF
ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBACbbA/5CA4Z5AhmOX4JMyxqXIh7JwR7B6S0DOFCj4k8Y8255K/bGXWw5tFWokSCi+89wnj7Y5AIrEnhMo9Pp2y3iQG21hFs+Ba0KI7cSL73X4bUBhLy1EUZjo5wNcPTNG1YgG94a9iTIoqUtoZLRiDvmPMvNR929dOTD5UEA3t3wy2XXg== nasp@milkplus
EOF
chown -R mide:mide ~mide/.ssh


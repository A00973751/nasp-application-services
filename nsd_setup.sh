#!/bin/bash - 
#===============================================================================
#
#          FILE: nsd_setup.sh
# 
#         USAGE: ./nsd_setup.sh 
# 
#   DESCRIPTION: NSD Configuration 
#               
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "NSD configuration starting..." 

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Student number invalid, please re-enter."
else
break
fi
done

echo "install nsd daemons" 
yum install -y nsd 

echo "configure nsd for eth0, 10.16.255.$StudentNumber, for outside traffics"
echo "configure /etc/nsd/nsd.conf file"
cat > /etc/nsd/nsd.conf <<EOF

server:        
        ip-address: 10.16.255.$StudentNumber        
        do-ip6: no
        include: "/etc/nsd/server.d/*.conf"
        include: "/etc/nsd/conf.d/*.conf"

remote-control:        
        control-enable: yes

# Fixed zone entries (i.e. not added with nsd-control addzone)

zone:
        name: "s$StudentNumber.as2016.learn"
        zonefile: "s$StudentNumber.as2016.learn.zone"

zone:
        name: "$StudentNumber.16.10.in-addr.arpa"
        zonefile: "$StudentNumber.16.10.in-addr.arpa.zone"

EOF

echo "configure DNS A records"
cat > /etc/nsd/s$StudentNumber.as2016.learn.zone <<EOF
;zone file for s$StudentNumber.as2016.learn
$TTL 10s                              ; 10 secs default TTL for zone
s$StudentNumber.as2016.learn.   IN  SOA  rtr.s$StudentNumber.as2016.learn. oadetiba.bcit.ca. (
                        2014022501    ; serial number of Zone Record
                        1200s         ; refresh time
                        180s          ; update retry time on failure
                        1d            ; expiration time 
                        3600          ; cache time to live
                        )
;Name servers for this domain
s$StudentNumber.as2016.learn.         IN      NS     rtr.s$StudentNumber.as2016.learn.

;MX Record
s$StudentNumber.as2016.learn.	  IN     MX    10 mail.s$StudentNumber.as2016.learn. 

;addresses of hosts
rtr.s$StudentNumber.as2016.learn.     IN      A      10.16.$StudentNumber.126
mail.s$StudentNumber.as2016.learn.    IN      A      10.16.$StudentNumber.1
EOF

echo "createing reverse lookup records"
cat > /etc/nsd/$StudentNumber.16.10.in-addr.arpa.zone <<EOF
;zone file for 10.16.$StudentNumber.0 / s$StudentNumber.as2016.learn reverse lookup
$TTL 10s; 10 secs default TTL for zone
$StudentNumber.16.10.in-addr.arpa.         IN      SOA  rtr.s$StudentNumber.as2016.learn. fletcher.my.bcit.ca. (

                               2016030101    ; serial number of Zone Record
                               1200s         ; refresh time
                               180s          ; retry time on failure
                               1d            ; expiration time 
                               3600          ; cache time to live
                               )

;Name servers for this domain
$StudentNumber.16.10.in-addr.arpa.         IN      NS      rtr.s$StudentNumber.as2016.learn.

;IP to Hostname Pointers
126.$StudentNumber.16.10.in-addr.arpa.        IN      PTR      rtr.s$StudentNumber.as2016.learn. 
1.$StudentNumber.16.10.in-addr.arpa.          IN      PTR     mail.s$StudentNumber.as2016.learn. 
;mail.s$StudentNumber.as2016.learn.           IN      MX     mail.s$StudentNumber.as2016.learn 
EOF

echo "Enabling & Starting NSD services"
systemctl enable nsd.service
systemctl restart nsd.service
systemctl status nsd.service

#!/bin/bash - 
#===============================================================================
#
#          FILE: loop_file_test-source.sh
# 
#         USAGE: ./loop_file_test-source.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Chad Fletcher 
#  ORGANIZATION: 
#       CREATED: 16/05/16 15:10
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# declares function "loop_test"
function loop_test {
  
# runs each array variable, declaring them as "var"
    for var in "$@"
    do
     if [ -e $var ]; then
       echo "The filename: $var exists"
     else
       echo "The filename: $var can't be found"
     fi
    done
  
  return
}


#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_main.sh
# 
#         USAGE: ./echo_main.sh 
# 
#   DESCRIPTION: echo's the first argument passed into the script. 
o#                included in the script: sourcing of a second script. 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: 
#       CREATED: 29/04/16 11:01
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


#sourcing activity


declare echo_main=$0
echo -e "\nBeginning $echo_main"

source ./simple_script.sh


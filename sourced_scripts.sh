#!/bin/bash


while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Invalid Number, Please Re-Enter."
else
break
fi
done

source ./vim_bash_setup.sh
source ./dhcpd_setup.sh
source ./unbound_setup.sh
source ./nsd_setup.sh
source ./ospf_setup.sh
source ./network_setup.sh
source ./wifi_setup.sh
source ./iptables_setup.sh

echo "Rebooting..."

reboot now

#!/bin/bash - 
#===============================================================================
#
#          FILE: unbound_setup.sh
# 
#         USAGE: ./unbound_setup.sh 
# 
#   DESCRIPTION: Unbound setup 
#                
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "Unbound configuration starting" 

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Student number invalid, Please Re-Enter."
else
break
fi
done

echo " Installing Unbound Daemons " 
yum install -y unbound

echo " configure unbound for interface eth1,10.16.$StudentNumber.126, s$StudentNumber.as2016.learn network"
cat > /etc/unbound/unbound.conf <<EOF
server:
        interface:10.16.$StudentNumber.126
        interface: 127.0.0.1
        port: 53
        do-ip4: yes
        do-ip6: no
        access-control: 0.0.0.0/0 refuse
        access-control: ::0/0 refuse
        access-control: 127.0.0.0/8 allow
        access-control: 10.16.$StudentNumber.0/25 allow
        chroot: ""
        username: "unbound"
        directory: "/etc/unbound"
#root-hints: "/etc/unbound/root.hints"
        pidfile: "/var/run/unbound/unbound.pid"
        private-domain: "ne.learn"
        private-domain: "htpbcit.ca"
        local-zone: "10.in-addr.arpa." nodefault
        local-zone: "16.172.in-addr.arpa." nodefault
        local-zone: "168.192.in-addr.arpa." nodefault
        prefetch: yes
        module-config: "iterator"
remote-control:
        control-enable: yes
        server-key-file: "/etc/unbound/unbound_server.key"
        server-cert-file: "/etc/unbound/unbound_server.pem"
        control-key-file: "/etc/unbound/unbound_control.key"
        control-cert-file: "/etc/unbound/unbound_control.pem"
include: /etc/unbound/conf.d/*.conf
stub-zone:
       name: "learn"
       stub-addr: 142.232.221.253
stub-zone:
       name: "htpbcit.ca"
       stub-addr: 142.232.221.253
stub-zone:
       name: "s$StudentNumber.as2016.learn"
       stub-addr: 10.16.255.$StudentNumber
stub-zone:
       name: "10.in-addr.arpa"
       stub-addr: 10.16.255.$StudentNumber
       stub-addr: 10.16.255.$StudentNumber
forward-zone:
        name: "bcit.ca"
        forward-addr: 142.232.221.253 
EOF

echo "Enabling & Starting Unbound services"
systemctl enable unbound.service
systemctl start unbound.service
systemctl status unbound.service

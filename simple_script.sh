#!/bin/bash - 
#===============================================================================
#
#          FILE: simple_script.sh
# 
#         USAGE: ./simple_script.sh 
# 
#   DESCRIPTION: Outputs the location of a command binary and returns an exit code
#                of zero if it is avaible or 127 if it can't be found
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 04/18/2016 15:22
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error

declare locations
declare whereis_output
declare ssh
read -p "Enter command you wish to find: " \
        ssh
whereis_output=$(whereis $cmd)
locations=$( (echo $whereis_output | cut -d ":" -f 2-) )
if [[ "$locations" == "" ]]; then
        echo "$ssh is not available!"
        echo "Exit code: 127"
else
        echo "$ssh is located in $locations"
        echo "Exit code: 0"
fi

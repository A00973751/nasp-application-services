#!/bin/bash - 
#===============================================================================
#
#          FILE: iptables_setup.sh
# 
#         USAGE: ./iptables_setup.sh 
# 
#   DESCRIPTION: IPTables setup
#                
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "IP Tables Configuration begins here..."

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Invalid number, Please Re-Enter."
else
break
fi
done

yum install -y iptables-services

cat > /home/iptables_setup.sh <<EOF
iptables -F -t filter
iptables -F -t nat

iptables -X

iptables -t filter -P INPUT DROP
iptables -t filter -A     INPUT   -i lo                -j ACCEPT
iptables -t filter   -A   INPUT  -i eth0 -d 10.16.255.$StudentNumber -p tcp --dport 22   -m state    --state NEW   -j ACCEPT
iptables -t filter  -A    INPUT  -i eth0    -d 10.16.255.$StudentNumber -p icmp    -j ACCEPT 
iptables -t filter -A     INPUT  -i eth0    -d 10.16.255.$StudentNumber -p tcp --dport 53 -m state      --state NEW -j ACCEPT
iptables -t filter -A     INPUT  -i eth0    -d 10.16.255.$StudentNumber  -p udp --dport 53   -j ACCEPT
iptables -t filter -A     INPUT  -m state      --state ESTABLISHED,RELATED     -j ACCEPT
iptables -t filter -A     INPUT  -i eth1       -s 10.16.$StudentNumber.0/25 -j ACCEPT
iptables -t filter -P OUTPUT ACCEPT
iptables -t filter -P FORWARD DROP
iptables -t filter -A     FORWARD -m state      --state ESTABLISHED,RELATED    -j ACCEPT
iptables -t filter -A     FORWARD -i eth1          -s 10.16.$StudentNumber.0/25   -j ACCEPT
iptables  -t filter -A     FORWARD -i eth0 -d 10.16.$StudentNumber.0/25 -p icmp   -j ACCEPT
iptables -t filter -A      FORWARD -i eth0  -d 10.16.$StudentNumber.1 -p tcp    --dport http   -j ACCEPT
iptables -t filter -A      FORWARD -i eth0  -d 10.16.$StudentNumber.1 -p tcp    --dport https  -j ACCEPT
iptables -t filter -A      FORWARD -i eth0  -d 10.16.$StudentNumber.1 -p tcp    --dport ssh    -j ACCEPT
iptables -t filter -A      FORWARD -i eth0  -d 10.16.$StudentNumber.1 -p tcp    --dport 50003  -j ACCEPT
iptables -t filter -A INPUT -i eth1 -d 10.16.255.$StudentNumber -p ospf -j ACCEPT
iptables -t filter -A INPUT -i eth1 -d 10.16.$StudentNumber.255 -p ospf -j ACCEPT
iptables -t filter -A INPUT -i wlp0s11u1 -d 10.16.255.$StudentNumber -p ospf -j ACCEPT
iptables -t filter -A INPUT -i wlp0s11u1 -d 10.16.255.$StudentNumber -p ospf -j ACCEPT
iptables -t filter -A INPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -A POSTROUTING -o eth0 -s 10.16.$StudentNumber.0/25 -j MASQUERADE
iptables -t nat -A POSTROUTING -o eth0 -s 10.16.$StudentNumber.128/25 -j MASQUERADE

EOF

chmod u+x iptables_setup.sh
bash /home/iptables_setup.sh
service iptables save

echo "Enabling & Starting firewalld"
systemctl stop firewalld.service
systemctl disable firewalld.service
systemctl status firewalld.service

echo "Enabling & Starting iptables services"
systemctl enable iptables.service
systemctl start iptables.service
systemctl status iptables.service

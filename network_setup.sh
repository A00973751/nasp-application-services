#!/bin/bash - 
#===============================================================================
#
#          FILE: network_setup.sh
# 
#         USAGE: ./natwork_setup.sh 
# 
#   DESCRIPTION: Network Configuration 
#                
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#        AUTHOR: Chad Fletcher
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION: 0.2
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "Network Interfaces Configuration starting..."

while true; do
read -p "Enter student number (Student number range between 1 to 24): "  StudentNumber

if  [[ "$StudentNumber" -lt "1" ]] || [[ "$StudentNumber" -gt "24" ]]; then
echo "Student number invalid, please re-enter."
else
break
fi
done

hostnamectl set-hostname s$StudentNumberrtr.as2016.learn

echo "Installing base packages"
yum -y update
echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
yum -y group install base

echo "Installing the Extra Packages for Enterprise Linux Repository" 
yum -y install epel-release
yum -y update

echo "Installing project specific tools"
yum -y install curl vim wget tmux nmap-ncat tcpdump nmap git

echo "Setting Up VirtualBox Guest Additions"
echo "Installing pre-requisities"
yum -y install kernel-devel kernel-headers dkms gcc gcc-c+

echo "Creating mount point, mounting, and installing VirtualBox Guest Additions"
mkdir vbox_cd
mount /dev/cdrom ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd

echo "Disabling selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config

echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service
systemctl disable firewalld.service

echo "Enabling and starting the Network Service (ignoring angry messages)"
systemctl enable network.service 
systemctl start network.service

echo "Setting up Instructor User"
useradd -m -G wheel,users instructor
echo "instructor ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
mkdir ~instructor/.ssh/
cat > ~instructor/.ssh/authorized_keys <<EOF
ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBACbbA/5CA4Z5AhmOX4JMyxqXIh7JwR7B6S0DOFCj4k8Y8255K/bGXWw5tFWokSCi+89wnj7Y5AIrEnhMo9Pp2y3iQG21hFs+Ba0KI7cSL73X4bUBhLy1EUZjo5wNcPTNG1YgG94a9iTIoqUtoZLRiDvmPMvNR929dOTD5UEA3t3wy2XXg== nasp@milkplus
EOF
chown -R instructor:instructor ~instructor/.ssh

echo "Configure IP address for interface eth0"
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 <<EOF
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
NAME=eth0
DEVICE=eth0
ONBOOT=yes
IPADDR=10.16.255.$StudentNumber
PREFIX=24
GATEWAY=10.16.255.254
DNS1=127.0.0.1
#DNS2=142.232.221.253
EOF

echo "Configure IP address for interface eth1"
cat > /etc/sysconfig/network-scripts/ifcfg-eth1 <<EOF
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=10.16.$StudentNumber.126
PREFIX=25
GATEWAY=10.16.255.$StudentNumber
EOF

echo "Enabling and restarting network service"
systemctl enable network.service
systemctl start network.service
systemctl status network.service

